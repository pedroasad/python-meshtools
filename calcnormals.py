# coding: utf-8
__doc__ = """\
Calculates per-vertex normals for a given point cloud given in PLY format.

Usage: %s <input.ply> <output.ply> <plane|poly> [k_neighbors]

Normals are calculated using a local surface model, which may consist in a
plane or a fourth degree polynomial. The number of neighbors may be chosen, but
no fewer than 3 points are used (5 is the default, and recommended). The output
file contains as many vertices as the input one but the only preserved
attributes are the vertex coordinates and normal coordinates. If the input file
contains mesh topology (i.e. faces), then this information is copied, as is, to
the output PLY file. Normal orientation is determined according to a simple
heuristic: by assuming that the model's shape is mostly convex and that its
center of mass is near the origin of the coordinate system, normals are
oriented such that the point away from the origin.
""" % __file__

import sys
import numpy as np
import numpy.linalg as npl
from plyfile import PlyData, PlyElement
from sklearn.neighbors import NearestNeighbors

def planeFitting():
    for i in xrange(nVerts):
        p = points[i]

        Q = pointBase[neigh[i],:]
        x, y, z = np.transpose(Q)

        # Permutes x, y, z in order to keep z as the least variance coordinate
        permut = np.argmin([np.var(x), np.var(y), np.var(z)])
        (x, y, z) = [
            (y, z, x),
            (x, z, y),
            (x, y, z)
        ][permut]

        # Builds the system and solves it
        mat = np.array([
            [sum(x*x), sum(x*y), sum(x)],
            [sum(x*y), sum(y*y), sum(y)],
            [sum( x ), sum( y ),    k  ]
        ])
        rhs = np.array([
            sum(x*z),
            sum(y*z),
            sum( z )
        ])
        ans = np.dot(rhs, npl.inv(mat))

        # The normal is calculated according the inverse permutation
        n = [
            np.array([-1, ans[0], ans[1]]),
            np.array([ans[0], -1, ans[1]]),
            np.array([ans[0], ans[1], -1])
        ][permut]

        n = n / npl.norm(n)

        # Orientation correction heuristic
        if np.dot(p, -n) > np.dot(p, n):
            n = -n

        # Normal data stored in PLY structure
        verts['nx'][i] = n[0]
        verts['ny'][i] = n[1]
        verts['nz'][i] = n[2]

def polynomialFitting():
    for i in xrange(nVerts):
        p = points[i]

        Q = pointBase[neigh[i],:]
        x, y, z = np.transpose(Q)

        #TODO: Not permuting coordinates according to least variance
        # Permutes x, y, z in order to keep z as the least variance coordinate
#        permut = np.argmin([np.var(x), np.var(y), np.var(z)])
#        (x, y, z) = [
#            (y, z, x),
#            (x, z, y),
#            (x, y, z)
#        ][permut]

        # Builds the system and solves it
        mat = np.array([
            [sum(x**4 * y**0), sum(x**3 * y**1), sum(x**2 * y**2), sum(x**3 * y**0), sum(x**2 * y**1), sum(x**2 * y**0)],
            [sum(x**3 * y**1), sum(x**2 * y**2), sum(x**1 * y**3), sum(x**2 * y**1), sum(x**1 * y**2), sum(x**1 * y**1)],
            [sum(x**2 * y**2), sum(x**1 * y**3), sum(x**0 * y**4), sum(x**1 * y**2), sum(x**0 * y**3), sum(x**0 * y**2)],
            [sum(x**3 * y**0), sum(x**2 * y**1), sum(x**1 * y**2), sum(x**2 * y**0), sum(x**1 * y**1), sum(x**1 * y**0)],
            [sum(x**2 * y**1), sum(x**1 * y**2), sum(x**0 * y**3), sum(x**1 * y**1), sum(x**0 * y**2), sum(x**0 * y**1)],
            [sum(x**2 * y**0), sum(x**1 * y**1), sum(x**0 * y**2), sum(x**1 * y**0), sum(x**0 * y**1), sum(x**0 * y**0)]
        ])
        rhs = np.array([
            sum(x**2 * z),
            sum(x * y * z),
            sum(y**2 * z),
            sum(x * z),
            sum(y * z),
            sum(z)
        ])
        ans = np.dot(rhs, npl.inv(mat))

        # The normal is calculated according the inverse permutation
#        n = [
#            np.array([-1, ans[0], ans[1]]),
#            np.array([ans[0], -1, ans[1]]),
#            np.array([ans[0], ans[1], -1])
#        ][permut]

        n = np.array([
            -(2 * ans[0] * p[0] + ans[1] * p[1] + ans[3]),
            -(2 * ans[2] * p[1] + ans[1] * p[0] + ans[4]),
            1
        ])
        n = n / npl.norm(n)

        # Orientation correction heuristic
        if np.dot(p, -n) > np.dot(p, n):
            n = -n

        # Normal data stored in PLY structure
        verts['nx'][i] = n[0]
        verts['ny'][i] = n[1]
        verts['nz'][i] = n[2]

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write(__doc__)
        sys.exit(1)

    # Argument parsing
    inputPath  = sys.argv[1]
    outputPath = sys.argv[2]
    mode = sys.argv[3]
    k = max(3, len(sys.argv) > 4 and int(sys.argv[4]) or 5)

    if mode not in ['plane', 'poly']:
        sys.stderr.write('Unknown mode: %s' % mode)
        sys.exit(1)

    # Opening input mesh
    inputMesh = PlyData.read(inputPath)
    nVerts = len(inputMesh['vertex'].data)

    # Output mesh preparation
    faces = inputMesh['face'].data
    verts = np.zeros(nVerts, dtype=zip(
        'x y z nx ny nz'.split(),
        [np.float32] * 6
    ))

    verts['x'] = inputMesh['vertex'].data['x']
    verts['y'] = inputMesh['vertex'].data['y']
    verts['z'] = inputMesh['vertex'].data['z']

    # Plain numpy array with the relevant points
    points = verts\
        .view(dtype=np.float32)\
        .reshape((nVerts, -1))\
        [:,:3]

    # Unique points for nearest neighbor searching
    pointBase = np.array(
        list(set(
            tuple(p) for p in points
        ))
    )

    # Nearest neighbor search
    search = NearestNeighbors(n_neighbors=k).fit(pointBase)
    neigh = search.kneighbors(points, return_distance=False)

    # Actual normal calculation process
    { 'plane' : planeFitting, 'poly' : polynomialFitting }[mode]()

    # Outputing results
    PlyData([
        PlyElement.describe(verts, 'vertex'),
        PlyElement.describe(faces, 'face'  )
    ], text=True).write(outputPath)
