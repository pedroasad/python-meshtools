# Introduction

This project implements some Python tools for 3D mesh processing, including:

* A mesh alignment tool for topoogically equivalent meshes
* A per-vertex normal calculator

Only Stanford PLY files (text encoding) are accepted as input.

## Mesh alignment

The `meshalign.py` script computes the optimal rigid transformation that maps
one mesh into another using the
concept of Singular Value Decomposition. The two input meshes must be in PLY
format and have the same number of vertices (corresponding vertices are matched
by order). Hence, most plausible results are obtained when the two input meshes
are very similar (corresponding to deformed instances of the same object, for
example) but may be used for different shapes as long as they have the same

## Normal calculator

The `calcnormals.py` script works by fitting local surface models (either linear
or polynomial) to the K nearest neighbors of each point in the given mesh.
Currently, only a plane local surface model is implemented. Run the script
without arguments to get a help message on its usage.

---

# Dependencies

* [NumPy](http://www.numpy.org/) (>= 1.11.0) - Array/matrix manipulation routines
* [scikit-learn](http://scikit-learn.org/stable/) (>= 0.18) - Used for nearest 
neighbor searching.
* [plyfile](https://github.com/dranjan/python-plyfile) (>= 0.4) - PLY file 
input/output.

---

# Acknowloedgements

* Thanks to [Blender 3D](http://www.blender.org), used for visualizing input and
output meshes and for providing the monkey 3D model used testing.
* Thanks to the [Stanford staff](http://graphics.stanford.edu/data/3Dscanrep/)
for the 3D models used in testing.

---

# Contact

* [E-mail](mailto:pasad@cos.ufrj.br)
* [Web page](http://www.pedroasad.com)
* [Gilab profile](https://gitlab.com/u/psa-exe)
